<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{

	protected $fillable=['title','content','agencie_id'];


	public function agencies() {

		return $this->belongsTo(Agencie::class);

    }

}
