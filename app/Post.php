<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

	protected $guarded = ['post_id'];

	protected $primrykey = 'post_id';

}
