<?php

namespace App\Providers;

use App\Events\TourCreated;
use App\Events\UserRegistred;
use App\Listeners\ApplyTour;
use App\Listeners\ApplyUserGift;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],

	    TourCreated::class =>[
	        ApplyTour::class
	    ],

	    UserRegistred::class =>[
	    	ApplyUserGift::class
	    ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
