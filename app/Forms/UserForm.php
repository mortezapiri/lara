<?php

namespace App\Forms;

use App\Role;
use Kris\LaravelFormBuilder\Form;

use App\User;

class UserForm extends Form
{
    public function buildForm()
    {
	    $this->add( 'name', 'text', [
		    'label' => 'نام',
	    ] )
	         ->add( 'email', 'email', [
		         'label' => 'ایمیل',
		         'required' => true,
	         ] )
		    ->add('password','password',[
			    'label' => 'پسورد'
		    ])
		    ->add('password_confirmation','password',[
			    'label' => 'پسورد'
		    ])
	         ->add( 'role_ids', 'choice', [
		         'label'   => 'تغییر نقش',
		         'rules' => 'required',
		         'required' => 'min:1',
		         'multiple' =>true,
		         'expanded' => true,
		         'choices' => $this->getRoles(),
	         ] )

	         ->add( 'submit', 'submit', [
		         'label' => 'ایجاد',
	         ]);
    }

	/**
	 * @return mixed
	 */
	private function getRoles() {

		$roles = Role::get()->pluck('name','id')->toArray();

		return $roles;

	}
}
