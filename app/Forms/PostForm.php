<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class PostForm extends Form
{
    public function buildForm()
    {
        $this->add('title','text',[
        	'label' => 'عنوان'
        ])
	        ->add('body','textarea',[
		        'label' => 'توضیحات'
	        ])
	        ->add('submit','submit',[
	        	'label' => 'ایجاد'
	        ]);

    }
}
