<?php

namespace App\Forms;

use App\Agencie;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\Form;

class ToursForm extends Form {
	public function buildForm() {

//    	$agencie= Agencie::all('title');

		$this->add( 'title', 'text', [
			'label' => 'عنوان تور',
			'required' => true,
		] )
		     ->add( 'content', 'textarea', [
			     'label' => 'توضیحات تور',
			     'required' => true,
		     ] )
		     ->add( 'agencie_id', 'choice', [
			     'choices' => $this->getAgencies(),
			     'required' => true,
			     'label'  => 'نام آژانس'
		     ] )

		     ->add( 'submit', 'submit', [
			     'label' => 'ایجاد تور',
		     ] );

	}

	/**
	 * @return mixed
	 */
	private function getAgencies() {

		$agencies = Agencie::get()->pluck( 'title', 'id' )->toArray();

		//dd($agencies);

		return $agencies;

	}






}
