<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class AgenciesForm extends Form
{
    public function buildForm()
    {
	    $this
		    ->add('title', 'text', [
			    'label' =>'نام آژانس',
			    'required' => true,
		    ])
		    ->add('about', 'textarea', [
			    'label' =>'توضیحات',
			    'required' => true,
		    ])
		    ->add('submit', 'submit',[
		    	'label' => 'ذخیره آژانس'
		    ]);
    }
}
