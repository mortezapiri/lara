<?php


namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\UrlShortners;
use Illuminate\Http\Request;


class UrlShortnerController extends Controller {

	public function generate() {
		return view( 'admin.urlshortner.generate' );

	}

	public function show() {


		$urls= UrlShortners::all();

		return view('admin.urlshortner.list',compact('urls'));


	}

	public function store( Request $request ) {

//		dd($request->all());

		$url_data = [

			'basic_url' => request()->input( 'basic_url' ),

			'short_url' => str_random( '6' ),

		];




		Urlshortners::create( $url_data );

//		return redirect()->back();

		return redirect()->route( 'admin.urlshortner' )->with( 'success', 'لینک کوتاه با موفقیت ساخته شد.' );


	}

	public function redirect($short_url) {

		dd($short_url);

	}

}
