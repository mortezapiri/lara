<?php

namespace App\Http\Controllers;

use App\Events\TourCreated;
use App\Forms\ToursForm;
use App\Http\Requests\TourRequest;
use App\Repositories\TourRepository;
use App\Tour;
use Collective\Html\FormBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;

class ToursController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		$toure = Tour::all();

		return view( 'admin.tours.index', compact( 'toure', 'agencie' ) );

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		$form = \FormBuilder::create( ToursForm::class, [
			'method' => 'POST',
			'url'    => route( 'tours.store' ),
		] );


		return view( 'admin.tours.create', compact( 'form' ) );

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( TourRepository $tourRepo, Request $request, TourRequest $tour_request ) {
		try {
			$tour = $tourRepo->create( $request );
			Event::fire( new TourCreated( $tour ) );
			flash( 'tour saved successfully' )->success();
			return redirect()->route( 'tours.edit', [ 'id' => $tour->id ] );
		} catch ( \Exception $exception ) {
			flash( $exception->getMessage() . '@' . $exception->getFile() . '#' . $exception->getLine() )->error();

			return redirect()->route( 'tours.index' );
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {

		$toure = Tour::findOrFail( $id );

		return view( 'admin.tours.show', compact( 'toure' ) );

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {

		$toure = Tour::findOrFail( $id );

		$form = \FormBuilder::create( ToursForm::class, [
			'method' => 'PUT',
			'url'    => route( 'tours.update', [ 'id' => $toure->id ] ),
			'model'  => $toure,
		] );


		return view( 'admin.tours.edit', compact( 'form' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {

		$toure = Tour::findOrFail( $id );

		$toure->update( $request->only( 'title', 'content' ,'agencie_id' ) );

		return redirect()->route( 'tours.edit', [ 'id' => $toure->id ] );

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
