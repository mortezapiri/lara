<?php

namespace App\Http\Controllers;

use App\Forms\ToursForm;
use App\Forms\UserForm;
use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;
use App\Role;
use App\User;
use App\role_user;
use Collective\Html\FormBuilder;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\Routing\Annotation\Route;

class UsersController extends Controller {
	/**
	 * @var UserRepository
	 */
	private $repository;

	/**
	 * UsersController constructor.
	 */
	public function __construct(UserRepository $repository) {
		$this->repository = $repository;
	}
	use FormBuilderTrait;


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		$users = User::all();

		return view( 'admin.users.list',compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

//		$form = \FormBuilder::create(UserForm::class, [
//			'method' => 'POST',
//			'url'    => route( 'users.store' ),
//		]);

		$form = $this->form(UserForm::class, [
			'method' => 'POST',
			'url'    => route( 'users.store' ),
		]);


		return view( 'admin.users.create', compact( 'form' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param UserRepository $user_repository
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(UserRequest $request) {

		$form = $this->form(UserForm::class);


		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$form->redirectIfNotValid();

		$user = $this->repository->create($request->only('name','email','password', 'role_ids'));

		return redirect()->route('users.edit',['id'=>$user->id]);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {

		$user = User::findOrFail( $id );

		$model = $user->toArray();
		$model['role_ids'] = $user->roles->pluck( 'id' )->toArray();


		$form = \FormBuilder::create( UserForm::class, [
			'method' => 'PUT',
			'url'    => route( 'users.update', [ 'id' => $user->id ] ),
			'model'  => $model,
		] );

		return view( 'admin.users.edit', compact( 'form' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( $id, Request $request) {

		$user = $this->repository->update( User::findOrFail( $id ), $request->only('name','email','password','role_ids'));

		return redirect()->route( 'users.edit', [ 'id' => $user->id ] );

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {

	}
}
