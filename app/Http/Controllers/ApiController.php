<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
	public function index() {
		$api = file_get_contents("https://jsonplaceholder.typicode.com/posts");

		json_decode($api);

		return view('api.api',compact('api'));

	}
}
