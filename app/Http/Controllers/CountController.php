<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;

class CountController extends Controller
{
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {


		$totalsPosts=[
			'posts' =>Post::count()
		];

		$totalsUsers=[
			'users' =>User::count()
		];


		return view('admin.Dashboard.index', compact('totalsPosts','totalsUsers'));

	}


}
