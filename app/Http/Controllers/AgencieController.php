<?php

namespace App\Http\Controllers;

use App\Agencie;
use App\Forms\AgenciesForm;
use App\Forms\ToursForm;
use App\Tour;
use Dotenv\Validator;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilder;

class AgencieController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */


	public function __construct() {

		if ( ! Auth::check() ) {
			return redirect()->route( 'login' );

		}

	}

	public function index() {

		$agencie = Agencie::all();

		return view( 'admin.agencies.index', compact( 'agencie' ) );

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */


	public function create() {

		$form = \FormBuilder::create( AgenciesForm::class, [
			'method' => 'POST',
			'url'    => route( 'agencies.store' )
		] );

		return view( 'admin.agencies.create', compact( 'form' ) );

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

		$agencie = Agencie::create( $request->only( 'title', 'about' ) );


		flash()->overlay( 'آژانس جدید با موفقیت ساخته شد', 'آژنس ساخته شد' );


		return redirect()->route( 'agencies.edit', [ 'id' => $agencie->id ] );

	}

	public function toure( Request $request, $id ) {

		$agencie = Agencie::findOrFail( $id );

		$form = \FormBuilder::create( ToursForm::class, [
			'method' => 'POST',
			'url'    => route( 'tours.store', [ 'id' => $agencie->id ] )
		] );


		return view( 'admin.tours.create', compact( 'form' ) );

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {


		$agencie = Agencie::findOrFail( $id );

		$toures = $agencie->tours;


		return view( 'admin.agencies.show', compact( 'agencie', 'toures' ) );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		$agencie = Agencie::findOrFail( $id );

		$form = \FormBuilder::create( AgenciesForm::class, [
			'method' => 'PUT',
			'url'    => route( 'agencies.update', [ 'id' => $agencie->id ] ),
			'model'  => $agencie,
		] );

		return view( 'admin.agencies.edit', compact( 'form' ) );

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {

		$agencies = Agencie::findOrFail( $id );

		$agencies->update( $request->only( 'title', 'about' ) );

		return redirect()->route( 'agencies.edit', [ 'id' => $agencies->id ] );


	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
