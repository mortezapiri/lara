<?php
/**
 * Created by PhpStorm.
 * User: Morteza
 * Date: 16/05/2018
 * Time: 07:20 AM
 */

namespace App\Repositories;


use App\Http\Controllers\realtimeController;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserRepository {

	public function create($ali) {

		$role_ids = $ali['role_ids'];

		$user = User::create( [
			'name' => $ali['name'],
			'email' => $ali['email'],
			'password' => Hash::make($ali['password']),
		] );

		$user->roles()->attach($role_ids);

		return $user;

	}


	public function update( User $user, $data ) {

		$role_ids = $data['role_ids'];
		unset($data['role_ids']);

		$user->update($data);

		$user->roles()->sync($role_ids);

		return $user;

	}

}