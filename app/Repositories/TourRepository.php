<?php

namespace App\Repositories;


use App\Tour;


class TourRepository {
	public function create( $request ) {
		$data = [
			'agencie_id' => (int) $request->input( 'agencie_id' ),
			'title'      => 'تور ' . $request->input( 'title' ),
			'content'    => $request->input('content'),
		];
		$tour = Tour::create($data);
		return $tour;
	}
}