<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agencie extends Model
{
	protected $fillable=['title','about'];

	public function tours() {
		return $this->hasMany(Tour::class);
	}
}
