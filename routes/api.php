<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::get('/users', function (){

	$users = \App\user::all();

	return $users;

});


Route::get('/posts', function (){

	$posts = \App\post::all();

	return $posts;

});


Route::get('urlshortners',function (){

	$urls = \App\UrlShortners::all();

	return $urls;

});