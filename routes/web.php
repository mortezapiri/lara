<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\AdminMiddleware;
use Illuminate\Support\Facades\Route;

Route::get( '/', function () {
	return view( 'welcome' );
} )->name( 'home' );


Route::get( '/admin',['middleware' => 'admin'], function () {
	return view( 'admin.dashboard.index' );

} );

Route::group( [ 'prefix' => 'admin', 'namespace' => 'admin','middleware' => 'admin'], function () {
	Route::get( '/urlshortner', 'UrlShortnerController@generate' )->name( 'admin.urlshortner' );
	Route::post( '/urlshortner', 'UrlShortnerController@store' )->name( 'admin.urlshortner' );
	Route::get( '/urlshortner/list', 'UrlShortnerController@show' )->name( 'admin.urlshortner.list' );
	Route::get( '/redirect/{code}', 'UrlShortnerController@redirect' );
} );


Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {
	Route::resource('users', 'UsersController');
	Route::resource('agencies', 'AgencieController');
	Route::resource('posts', 'PostsController');
	Route::get('/agencies/{id}/tour', 'AgencieController@toure')->name('create.tour');
	Route::resource('tours', 'ToursController');
	Route::get('/','countController@index')->name('admin');
});



//Route::resource('agencies', 'AgencieController');


//Route::get( '/posts', 'PostController@index' );
//Route::get( '/posts/create', 'PostController@create' )->name( 'admin.posts.create' );
//Route::post( '/posts/create', 'PostController@store' );
//Route::get( '/posts/{post_id}', 'PostController@show' )->name( 'post.show' );



Route::get( '/realtime', 'realtimeController@test' );


Auth::routes();

Route::get( '/home', 'HomeController@index' );


Route::get( '/api', 'ApiController@index' );





