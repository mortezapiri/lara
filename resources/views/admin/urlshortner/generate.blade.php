@include('admin.Partials.header')
@extends('layout.admin')
@include('admin.Partials.sidebar')

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">

                    <form role="form" method="post">

                        {{ csrf_field() }}

                        <div class="form-group">

                            @include('admin.Partials.alert')


                            <label for="generate_url">ساخت لینک جدید</label>
                            <input type="text" class="form-control" id="basic_url" name="basic_url"placeholder="http://mrpiri.ir/#contact">

                            <hr>

                            <button type="submit" class="btn btn-primary" name="generate_url_btn">کوتاه کردن لینک</button>

                        </div>




                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.box -->

@endsection
