@include('admin.Partials.header')
@extends('layout.admin')
@include('admin.Partials.sidebar')

@section('title')
    لیست کاربران
@endsection

@include('admin.Partials.alert')
@section('content')

    @if($users && count($users) > 0)

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-9">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">لیست کاربران</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>شناسه</th>
                                    <th>نام کامل</th>
                                    <th>ایمیل</th>
                                    <th style="text-align: center">نقش کاربری</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>

                                @foreach($users as $user)

                                    <tr>
                                        <td> {{  $user->id }} </td>
                                        <td> {{  $user->name}} </td>
                                        <td>{{  $user->email }}</td>

                                        <td style="text-align: center">
                                            @foreach($user->roles as $rols)
                                                <p class="label label-success">
                                                {{ $rols->name }}
                                                </p>
                                            @endforeach
                                        </td>

                                        <td style="text-align: center">
                                            <a href="{{ route('users.edit',$user->id) }}">
                                                <li class="glyphicon glyphicon-edit"></li>
                                            </a>
                                            <a href="{{  route('users.destroy',$user->id)  }}">
                                                <li class="glyphicon glyphicon-trash"></li>
                                            </a>

                                        </td>


                                    </tr>

                                @endforeach


                                <tfoot>
                                <tr>
                                    <th>شناسه</th>
                                    <th>نام کامل</th>
                                    <th>ایمیل</th>
                                    <th style="text-align: center">نقش کاربری</th>
                                    <th>عملیات</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
        </div>

        </table>

    @else
        <h1>کاربری برای نمایش وجود ندارد</h1>
        <a href="/admin/users/create">
            <button class="form-control btn-success col-md-4">اضافه کردن کاربر جدید</button>
        </a>
    @endif




@endsection

@include('admin.Partials.footer')