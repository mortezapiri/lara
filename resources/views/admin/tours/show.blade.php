@extends('layout.admin')

@section('content')

    <section class="content">

        <!-- Default box -->
        <h5>مشخصات تور</h5>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $toure->title }}</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="جمع شود">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="حذف">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                {{ $toure->content }}
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer-->
        </div>


    </section>

@endsection