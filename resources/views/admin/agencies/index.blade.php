@extends('layout.admin')

@section('content')




    <section class="content">
        <div class="row">
            <div class="col-xs-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">لیست آژانس ها</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>نام آژانس</th>
                                <th>تور ها</th>
                            </tr>
                            </thead>
                            @foreach($agencie as $agencie)

                            <tr>
                                <td> {{  $agencie->id }} </td>
                                <td>
                                    <a href="{{ route('agencies.show',$agencie->id) }}">
                                    {{  $agencie->title }}
                                    </a>
                                </td>

                               <td><a href="{{ route('tours.create',array('agencie_id'=>$agencie->id)) }}">افزودن تور</a></td>

                            </tr>


                            @endforeach

                            <tfoot>
                            <tr>
                                <th>شناسه</th>
                                <th>نام آژانس</th>
                                <th>تور ها</th>

                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>


@endsection