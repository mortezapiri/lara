@extends('layout.admin')

@section('content')

    <section class="content">

        <!-- Default box -->
        <h5>نام آژانس</h5>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $agencie->title }}</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="جمع شود">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="حذف">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                {{ $agencie->about }}
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
        <h4>تور ها</h4>
@foreach($toures as $toure)

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $toure->title  }}</h3><br>
                <h3 class="box-title">{{ $toure->content  }}</h3>
            </div>
            <div class="box-body">
                {{--{{ $agencie->about }}--}}
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer-->
        </div>

    @endforeach

    </section>

@endsection