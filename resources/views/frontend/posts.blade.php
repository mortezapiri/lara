@extends('layouts.app')


@section('content')

    <div class="container">




        @foreach($posts as $post)

        <div class="panel panel-success">
            <div class="panel-heading">

                <a href="{{ route('post.show',$postId) }}">{{ $post->post_title  }}</a>

            </div>
            <div class="panel-body">
                {{$post->post_body}}
            </div>
        </div>

        @endforeach




    </div>



@endsection